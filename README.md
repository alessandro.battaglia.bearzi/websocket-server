# WebSocket Server

Questo è un esempio di server WebSocket creato con Node.js e Docker.

## Installazione

Per utilizzare il server, è necessario avere Docker e Docker Compose installati sul proprio computer.

1. Clonare il repository sul proprio computer.
2. Nella cartella del progetto, eseguire il comando `docker-compose up` per creare e avviare il contenitore Docker del server WebSocket.

## Utilizzo

Il server WebSocket è in ascolto sulla porta 8080. Per utilizzarlo, è possibile creare un client WebSocket utilizzando qualsiasi linguaggio di programmazione che supporti WebSocket.

Di seguito è riportato un esempio di client WebSocket in JavaScript:

```javascript
const WebSocket = require('ws');

const ws = new WebSocket('ws://localhost:8080');

ws.on('open', () => {
  console.log('Connessione WebSocket aperta.');
});

ws.on('message', (data) => {
  console.log('Messaggio ricevuto:', data);
});

ws.on('close', () => {
  console.log('Connessione WebSocket chiusa.');
});

ws.send('Ciao, server!');
```

In questo esempio, il client WebSocket si connette al server WebSocket in ascolto sulla porta 8080, invia un messaggio "Ciao, server!" e ascolta i messaggi in arrivo dal server. È possibile 
modificare il client WebSocket per inviare e ricevere i messaggi desiderati.

## Contributi

I contributi sono benvenuti! Per segnalare un bug o inviare una richiesta di pull, utilizzare la pagina del repository su GitHub.

## Licenza

Questo progetto è distribuito con licenza MIT.
