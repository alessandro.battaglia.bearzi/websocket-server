# WebSocket Server (EN)

This is an example WebSocket server built with Node.js and Docker.

## Installation

To use the server, you will need to have Docker and Docker Compose installed on your machine.

1. Clone the repository to your computer.
2. In the project directory, run the command `docker-compose up` to create and start the WebSocket server Docker container.

## Usage

The WebSocket server is listening on port 8080. To use it, you can create a WebSocket client using any programming language that supports WebSocket.

Here is an example WebSocket client in JavaScript:

```javascript
const WebSocket = require('ws');

const ws = new WebSocket('ws://localhost:8080');

ws.on('open', () => {
  console.log('WebSocket connection opened.');
});

ws.on('message', (data) => {
  console.log('Received message:', data);
});

ws.on('close', () => {
  console.log('WebSocket connection closed.');
});

ws.send('Hello, server!');
```

In this example, the WebSocket client connects to the WebSocket server listening on port 8080, sends a "Hello, server!" message, and listens for incoming messages from the server. You can modify the 
WebSocket client to send and receive the messages you want.

## Contributing

Contributions are welcome! To report a bug or submit a pull request, please use the repository page on GitHub.

## License

This project is licensed under the MIT License.
